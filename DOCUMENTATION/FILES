Here's a little list of all the important files of the project and what you need to know about it. Note that the source files (.c) will be in src/ directory and the header ones (.h) in inc/.


- About Sockets
***************

## socket.h
-----------

Probably the most important header file, because it contains the definition of the socket structure used by every each function of the library.
As you can see if you open this file, the structure fields remains pretty simple :
* fd : is actually the return value of the call to socket(2) made when creating the socket.

* type : an enumerated value you must specify at the socket's creation, used by such functions like enable_socket to know what they have to do (enable a server socket isn't the same than enabling a client one !)

* family : what is family and why do we keep it is a good question. It defines the "family" of the connection you want to create : in a network context, most usual values are AF_INET (IPv4) and AF_INET6 (IPv6). It is, in addition, the first argument of the socket syscall. So why do we keep it ? Two reasons for that :
  1) first one is that, server like client-side, we need to know of which family the socket is to enable it. More precisely, the enable_socket function contains a struct sockaddr_in object that needs it, in both server and client cases, so, I found it more convenient to keep it inside the socket structure instead of passing it as a parameter to enable_socket ;
  2) second reason : when a server socket accepts a new client, it consequently knows of which family the client socket should be, as it knows its own one. Convenience, once again.

* in and out : in and out are the two ring buffers of the socket. They are to be used for safe transmissions : read_from_socket will store what it read in the "in" buffer, and send_from_socket will seek to send what's in the "out" buffer. See the part about ring_buffer.c and ring_buffer.h below for more information.



## socket.c
-----------
The matching source file of socket.h. It contains all the useful functions in the life of a socket. Let's see them in theorical order of calling :

    1) create_socket

       Returns : a pointer to a freshly allocated t_sock structure pointer
       Parameters : char *protocol, int family, int com, socket_type type
       Description :
       The first function to call when creating a brand new socket.
       The function parameters are merely the ones of the socket syscall.
       protocol : represents the name of an Unix socket-compatible protocol (to do TCP/IP, one should give "TCP").
       family : please refer to the explaination of socket.h upper.
       com : another parameter of socket(2). It represents the type of communication you want to provide. All possible values of family and com are available in the socket(2) man page.
       type : is an enumerated, boolean-style type of the library declared in socket.h, and only serves to functions like enable_connect to know if they must behave in a server or client context.
       Note that it calls another function, initialize_socket, which is mainly used to, well, initialize the fields of the structure (since the author is a fierce paranoiac of Valgrind's warnings about uninitialized values).
       ATTENTION : note that for memory economy reasons, the socket's communication ring buffers are NOT allocated by default (e.g., a server socket has no use of a communication buffer, and it would be a waste of memory to allocate it). See below about ring buffers to know how to use create_communication_buffers, a flexible function that will allow you to create the socket's ring buffer(s).

       	    
    2) enable_socket

       Returns : int value to know if there was an error

       Parameters : t_sock *socket, char *ip, int port, int backlog

       Description :
       Suppose we have created a socket. We now need to enable it. But enabling a server socket isn't like enabling a client one. So, yes, this function IS here to give the illusion that it is the same. In both cases, the port parameter is used by a struct sockaddr_in object to permit the connection.
       But let's begin with the server part :
       - in order to connect a server socket and make it available for listening to new client, you barely have to do two things : bind(2) and listen(2). This is, indeed, precisely what this function does with a server socket. See the man pages to know what exactly these two syscalls are for. IP remains unused for a server utilization. "backlog" is an argument of listen which represents the maximum length of the pending connections' queue. It is usually set to SOMAXCONN (i.e. : 128), but unless you're in a very special context, this value doesn't really matter.
       - the client part is more simple : all we have to do is to connect(2) (see the man pages) the socket to the specified IP, and it should automagically work from here.
	

    3) accept_socket

       Returns : a newly allocated pointer to a new client's socket structure (or NULL if error)

       Parameters : t_sock *server, size_t capacity, size_t size

       Description :
       As its name lets suggest it, this function is specifically server-side. It permits a server socket to accept a new client when it knows there is one, e.g. when, after a return of select(2), the server socket fd is set in the read FD_SET. It consequently makes an call to accept(2) to retrieve the client fd, and creates a new socket structure out of it. capacity and size are for the new client structure : it will be the capacity, in number of "data blocks", and the size of these data blocks, into the ring buffers of the client socket. We could set it to the capacity and size of the server socket by default, but we chose to let a maximum flexibility to the user.
       ATTENTION : an important usage advice is to NOT allocate communication buffers of the newly accepted socket : this function does it for you ! As an accepted socket, unlike other sockets, we assume it has to be a client socket, and so, you will want to read and write on it. So, we do it. Just be careful not to re-do it.



    4) destroy_socket

       Returns : int to know if there was an error

       Parameters : t_sock *socket

       Description :
       Function to call when you're done with a socket and want to delete it. Destroys the given socket and, before that, all the fields inside.



- About Ring Buffers (ring_buffer.c, ring_buffer.h, communication.c, communication.h)
********************


* What is a ring buffer and how are they implemented in library
---------------------------------------------------------------

If you're willing to use this library, it is important for you to know how ring buffers work, as they are heavily used by the lib for all data transfers.

Read first the well-done Wikipedia page about it : http://en.wikipedia.org/wiki/Circular_buffer .

Long story short, a ring buffer is a buffer composed of "x" blocks of "y" size. Each input operation "uses" a block, and each output operation "frees" a block.
This way, when we reach the "end" of the buffer, we come back at the beginning and overwrite the "first" data block, and so on...

So, how are ring buffers implemented in the library ? Here are the used values for a ring buffer (relevant source code is located in inc/ring_buffer.h):

    - start : start of the data (it's the pointer which will be allocated !)
    - end : the end pointer of the buffer. When we reach it -> go back to start
    - capacity : the number of "blocks" we want
    - count : the number of "blocks" used at the moment (empty buffer -> count = 0)
    - size : the "size" in bytes of each data block in the buffer. 
    - offset : Only used for sending over network. When we send something, we may not send as many as we wanted for any reason. So we keep how much we sent, and only pass to the next block if (offset == size) (or message was smaller than the block size)
    - head : the head of the buffer points to the start of the current available block to write on.
    - tail : the tail of the buffer points to the start of the current block to send data from.


* How to use ring buffers
-------------------------

The one thing to know about the MLN (MiniLibNetwork)'s ring buffers is that they are NOT allocated by default, mainly for memory savings reasons. e.g. : a server socket has absolutely no use of communication buffers !
So, when you create a socket, you have to allocate these buffers. This can be easily done : look at communication.c & ring_buffer.c.

* create_communication_buffers
------------------------------
To give your socket communication buffers, use this function with these parameters:
   - socket (the socket)
   - value : an enumerated value which can take three values : IN, OUT, BOTH. Use this to specify which communication buffer(s) you want to alloc : the input buffer (for reading), the output one (for sending), or both. e.g. : you could want only to listen to your socket (and never answer), or just send data to your socket without ever listening to it... It lets you a great flexibility about how you want to use your socket !
   - capacity & size : as explained upper, they are the number of data blocks and the size of these. One could for example make a buffer of 10 (capacity) blocks of 128 (size) bytes.


* rb_push_back
--------------

Now you have communication buffers, you may want to put some data in them.
The rb_push_back function is here for it. Simply give it the buffer and the item you want to put in the buffer, e.g. rb_push_back(sock->in, "42") and it will store whatever you gave it in the head block of the buffer, unless it is already full.

NOTES
-----

Don't use it on a full buffer (it does nothing and returns an error).

Be EXTREMELY CAREFUL about pushing in data (usually strings) that DOESN'T EXCEED THE SIZE OF A BLOCK, which otherwise would result in the loss of the end of your message (push "hello" in a size 4 block, and you'll lose the 'o').

Also note that all blocks are separated by a end-of-string character ('\0').

This function increments the count variable of the buffer.


* rb_pop_front
--------------

If for some reason you want to remove the data in the tail block, just call rb_pop_front with your buffer and some item in which you would want to store the data, e.g. rb_pop_front(socket->in, storage_string). It will copy the content of the block into the item before erasing it. If you pass NULL as item, the data is simply deleted without save.

NOTES
-----

Don't use it on an empty buffer.

This function decrements the count variable of the buffer.


* read_from_socket
------------------

When you want to read incoming data from a socket, you have to call this function with parameters :
     - socket: the socket
     - flags : the flags you could want to pass to recv(2), as it's the syscall this function uses.

NOTES
-----

The function will always try to read the size of one ring buffer's block, and go the next one.
Consequently, it may not always entirely fill the block, but pass to the next one anyway. Which is probably sad.

This function increments the count variable of the buffer.


* send_to_socket
----------------
Sending is a bit more complicated than reading.
To send something, you must first put something in your buffer (yeah, really).
To do so, begin by using the rb_push_back function on your buffer, for example : rb_push_pack(socket->out, "something_very_interesting").
Now you have data to send in your buffer, you have to call the send_to_socket function with parameters :
     - socket: the socket
     - flags : the flags you could want to pass to send(2), as it's the syscall this function uses.

NOTES
-----

This function always tries to send the entire size of a block (here, the tail block).
If it didn't succeed to send everything in one attempt (send(2) returned a smaller value than outbuf->size), the buffer stores the 'offset', i.e. the place of the beginning of the unsent part in the data block, to send it the next time.
The offset is reset when it reaches outbuf->size, OR that the current character on which the offset is is a '\0' : this simply means the message was smaller than the block size, and that we sent everything.

This function decrements the count variable of the buffer.



- Logging System (log.c/.h)
****************

The library comes with a simple logging system which you can [en-dis]able via the net_log function.
Just call once the net_log function with the code LOG_ON to make the library write in a file called "libnetwork.log" in the directory where your program executable is. Note that you also can pass the option LOG_ON_APPEND to make it append to the file instead of truncating it. Send LOG_OFF to the net_log function to disable logging.



- Error Printing (net_perror.c / .h)
****************
The library also provides its own error printing system, which can be useful for library-specific errors that can occur, e.g. when you try to push back in a full buffer or to pop out in an empty buffer.