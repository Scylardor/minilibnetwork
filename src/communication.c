/* communication.c for my_libnetwork in /home/baron_a/Projects/my_libnetwork */

/* Made by Alexandre Baron */
/* Login   <baron_a@epitech.net> */

/* Started on  Sun Sep  2 19:58:53 2012 Alexandre Baron
** Last update Wed Sep 12 21:35:34 2012 Alexandre Baron
*/
/* Copyright (C) 2012 Baron Alexandre */
/* 	      This program is free software; you can redistribute it and/or */
/* modify it under the terms of the GNU Lesser General Public */
/* 	      License as published by the Free Software Foundation; either */
/* 	      version 2.1 of the License, or (at your option) any later version. */

/* 		   This program is distributed in the hope that it will be useful, */
/* 		   but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU */
/* Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public */
/* 	      License along with this program; if not, write to the Free Software */
/*   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA */

#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <string.h>

#include "values.h"
#include "net_perror.h"
#include "globals.h"
#include "ring_buffer.h"
#include "socket.h"
#include "log.h"

/* Function to create communication buffers of the socket. */
/* "value" is an enum defined in socket.h, letting you choose */
/* which buffer you want to have : in one (listening only), */
/* out one (sending only), or both. */
/* This system avoids the user to systematically have communication buffers, */
/* e.g. within a server socket (which is useless), to consume less memory. */
/* Don't forget to check that socket != NULL */
int			create_communication_buffers(t_sock *socket, buff_to_alloc value, size_t capacity, size_t size)
{
  extern net_code	g_code;

  if (socket == NULL)
    {
      g_code = NET_INVALID_ARG;
      write_to_log("create_socket_comm_buffers", "Invalid argument:", "Null socket");
      return (NET_FAILURE);
    }
  socket->in = NULL;
  socket->out = NULL;
  if (value == BOTH || value == IN)
    socket->in = rb_create(capacity, size);
  if (value == BOTH || value == OUT)
    socket->out = rb_create(capacity, size);
  if (((value == IN || value == BOTH) && socket->in == NULL) ||
      ((value == OUT || value == BOTH) && socket->out == NULL))
    {
      g_code = NET_MALLOC;
      write_to_log("create_socket_comm_buffers", "malloc:", "Virtual memory exhausted");
      return (NET_FAILURE);
    }
  return (NET_SUCCESS);
}

/* Another version of "rb_push_back" function (communication-oriented). */
/* Parameters : */
/*   - fd : the fd on which we read */
/*   - in : the ring buffer of the read socket */
/*   - flags : the flags user wants to pass to recv */
/* - verifies that the buffer is not full */
/* - memset the part where it's gonna read with zeros */
/* - read */
/* - jump to the next part of ring buffer (if to the end, go back to start) */
/* Unlike other functions of the lib, read_from_socket returns the number of */
/* bytes read. So a return of "0" will mean the client properly disconnected */
int			read_from_socket(t_sock *sck, int flags)
{
  int			ret;
  extern net_code	g_code;

  if (sck->in->count == sck->in->capacity)
    {
      g_code = NET_FULLBUFF;
      write_to_log("read_from_socket", "Full buffer:", "Cannot read more");
      return (NET_FAILURE); // FULL BUFF
    }
  memset(sck->in->head, '\0', sck->in->size);
  ret = recv(sck->fd, sck->in->head, sck->in->size, flags);
  if (ret == -1)
    {
      g_code = NET_RECV;
      write_to_log("read_from_socket", "recv:", strerror(errno));
      return (NET_FAILURE);
    }
  write_to_log("read_from_socket", "Successfully read a block of data", NULL);
  sck->in->head = (void *)sck->in->head + sck->in->size + 1;
  if (sck->in->head == sck->in->end)
    sck->in->head = sck->in->start;
  sck->in->count++;
  return (ret);
}

/* Another version of "rb_pop_out" function (communication-oriented). */
/* Parameters : */
/*   - fd : the fd on which we read */
/*   - out : the ring buffer of the socket we will write on */
/*   - flags : the flags user wants to pass to send */
/* - verifies that the buffer is not empty */
/* Sending is a little bit more tricky than receiving. */
/* We have to keep in mind that everything we send may not be totally sent in one time. */
/* So we keep a 'offset' variable in the ring buffer, that we increment each time we write. */
/* if offset == size (we come to the end of this part) OR we come to a '\0' (we wrote */
/* everything but the message was small), reset offset to 0 and pass to the next part */
/* - memset the part where it's gonna read with zeros */
/* - read */
/* - jump to the next part of ring buffer (if to the end, go back to start) */
int			send_to_socket(t_sock *sck, int flags)
{
  int			ret;
  extern net_code	g_code;

  if (sck->out->count == 0)
    {
      g_code = NET_EMPTYBUFF;
      write_to_log("send_from_socket", "Empty buffer:", "Nothing to send");
      return (NET_FAILURE); // EMPTY BUFF
    }
  ret = send(sck->fd, (char *)(sck->out->tail + sck->out->offset), strlen((char *)(sck->out->tail + sck->out->offset)), flags);
  if (ret == -1)
    {
      g_code = NET_SEND;
      write_to_log("send_from_socket", "send:", strerror(errno));
      return (NET_FAILURE);
    }
  sck->out->offset += ret;
  if (sck->out->offset == sck->out->size || ((char *)(sck->out->tail + sck->out->offset))[0] == '\0')
    {
      write_to_log("send_to_socket", "Successfully sent a block of data", NULL);
      sck->out->offset = 0;
      sck->out->count--;
      memset(sck->out->tail, 0, sck->out->size);
      sck->out->tail = (char *)sck->out->tail + sck->out->size + 1;
      if (sck->out->tail == sck->out->end)
	sck->out->tail = sck->out->start;
    }
  return (NET_SUCCESS);
}
