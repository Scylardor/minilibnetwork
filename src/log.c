/*
** log.c for libnetwork in /home/baron_a/Projects
**
** Made by Alexandre Baron
** Login   <baron_a@epitech.net>
**
** Started on  Tue Sep  4 17:47:26 2012 Alexandre Baron
** Last update Mon Sep 10 21:37:45 2012 Alexandre Baron
*/
/* Copyright (C) 2012 Baron Alexandre */
/* 	      This program is free software; you can redistribute it and/or */
/* modify it under the terms of the GNU Lesser General Public */
/* 	      License as published by the Free Software Foundation; either */
/* 	      version 2.1 of the License, or (at your option) any later version. */

/* 		   This program is distributed in the hope that it will be useful, */
/* 		   but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU */
/* Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public */
/* 	      License along with this program; if not, write to the Free Software */
/*   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA */


#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#include "net_perror.h"
#include "values.h"
#include "log.h"

static int g_log; // fd of the log file, > 0 if open, static because used only in this file

void		write_to_log(char *func, char *what, char *opt)
{
  if (g_log > 0)
    dprintf(g_log, "%s: %s %s\n", func, what, (opt ? opt : ""));
}

int		net_log(net_log_code code)
{
  extern int	g_code;

  if ((code == LOG_ON || code == LOG_ON_APPEND) && g_log == 0)
    {
      if (code == LOG_ON)
	g_log = open("libnetwork.log", O_WRONLY | O_TRUNC | O_CREAT, 0644);
      else
	g_log = open("libnetwork.log", O_WRONLY | O_APPEND | O_CREAT, 0644);
      if (g_log == -1)
	{
	  g_code = NET_READ;
	  return (NET_FAILURE);
	}
      dprintf(g_log, "* Start logging of libnetwork functions. *\n");
    }
  if (code == LOG_OFF && g_log > 0)
    {
      dprintf(g_log, "* Libnetwork logging system disabled *\n");
      close(g_log);
      g_log = 0;
    }
  return (NET_SUCCESS);
}
