/*
** ring_buffer.c for Ring Buffer Library in /home/barona/ring_buffer
**
** Made by baron_a
** Login   <baron_a@epitech.net>
**
** Started on  Thu Aug 30 10:05:56 2012 Alexandre Baron
** Last update Wed Sep 12 20:50:47 2012 Alexandre Baron
*/
/* Copyright (C) 2012 Baron Alexandre */
/* 	      This program is free software; you can redistribute it and/or */
/* modify it under the terms of the GNU Lesser General Public */
/* 	      License as published by the Free Software Foundation; either */
/* 	      version 2.1 of the License, or (at your option) any later version. */

/* 		   This program is distributed in the hope that it will be useful, */
/* 		   but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU */
/* Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public */
/* 	      License along with this program; if not, write to the Free Software */
/*   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA */

/* With Special Thanks to "Adam Rosenfield" of Stack Overflow */
/* for having shared the base of my ring buffer system. */

#include <string.h>

#include "net_perror.h"
#include "ring_buffer.h"
#include "values.h"

/* Function to create a new ring buffer. */
/* - Allocates enough space (size of a "data slice" * number of wanted slices) */
/* - Correctly set each pointer and value of the buffer */
/* Remember to check the return value in case of malloc failure */
/* NOTE : the "+ capacity" you see are to allocate as many '\0's as there */
/* will be data blocks. */
t_ring		*rb_create(size_t capacity, size_t size)
{
  t_ring	*buff;

  if ((buff = malloc(sizeof(*buff))) == NULL)
    return (NULL);
  buff->start = malloc((capacity * size) + capacity);
  if (buff->start == NULL)
      return (NULL);
  memset(buff->start, 0, (capacity * size) + capacity);
  buff->end = (void *)buff->start + (capacity * size) + (capacity - 1);
  buff->capacity = capacity;
  buff->count = 0;
  buff->size = size;
  buff->head = buff->start;
  buff->tail = buff->start;
  buff->offset = 0;
  return (buff);
}

/* Pretty simple */
void	rb_free(t_ring *buff)
{
  if (buff)
    {
      if (buff->start)
	free(buff->start);
      free(buff);
    }
}

/* This function pushes back a data slice in the ring buffer and, thus: */
/* - should not be called if count == capacity (full buffer) */
/* - advances the buffer head */
/* - if the head reaches the end -> goes back to the beginning */
/*   Note : be sure not to pass null pointer into item ! */
int	rb_push_back(t_ring *buff, const void *item)
{
  if (buff == NULL || item == NULL)
    return (NET_FAILURE);
  if (buff->count == buff->capacity)
    return (NET_FULLBUFF); // FULLBUFF
  memcpy(buff->head, item, buff->size);
  buff->head = (void *)buff->head + buff->size + 1;
  if (buff->head == buff->end)
    buff->head = buff->start;
  buff->count++;
  return (NET_SUCCESS);
}

/* This function pops out a data slice from the ring buffer and, thus: */
/* - should not be called if count == 0 (nothing in the buff) */
/* - advances the buffer tail */
/* - if the tail reaches the end -> goes back to the beginning */
/*   Note : you can store popped out data in item; */
/* but since we verify it isn't NULL, if you don't care about storing it, */
/* you can just pass NULL instead */
int	rb_pop_front(t_ring *buff, void *item)
{
  if (buff == NULL)
    return (NET_FAILURE);
  if (buff->count == 0)
    return (NET_EMPTYBUFF); //  EMPTYBUFF
  if (item != NULL)
    memcpy(item, buff->tail, buff->size);
  memset(buff->tail, 0, buff->size + 1);
  buff->tail = (void *)buff->tail + buff->size + 1;
  if (buff->tail == buff->end)
    buff->tail = buff->start;
  buff->count--;
  return (NET_SUCCESS);
}
