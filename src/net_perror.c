/*
** net_errormsg.c for my_libnetwork in /home/baron_a/Projects/my_libnetwork
**
** Made by Alexandre Baron
** Login   <baron_a@epitech.net>
**
** Started on  Sun Sep  2 19:06:42 2012 Alexandre Baron
** Last update Mon Sep 10 21:38:06 2012 Alexandre Baron
*/
/* Copyright (C) 2012 Baron Alexandre */
/* 	      This program is free software; you can redistribute it and/or */
/* modify it under the terms of the GNU Lesser General Public */
/* 	      License as published by the Free Software Foundation; either */
/* 	      version 2.1 of the License, or (at your option) any later version. */

/* 		   This program is distributed in the hope that it will be useful, */
/* 		   but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU */
/* Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public */
/* 	      License along with this program; if not, write to the Free Software */
/*   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA */


#include <stdio.h>

#include "values.h"
#include "net_perror.h"

void			net_perror()
{
  extern net_code	g_code;

  fprintf(stderr, "libnetwork: ");
  switch (g_code)
    {
    case NET_INVALID_ARG:
      fprintf(stderr, "invalid argument (null pointer ?)\n");
      break;
    case NET_FULLBUFF:
      fprintf(stderr, "buffer full (while attempting to push in)\n");
      break;
    case NET_EMPTYBUFF:
      fprintf(stderr, "empty buffer (while attempting to pop out)\n");
      break;
    case NET_MALLOC:
      fprintf(stderr, "virtual memory exhausted (malloc failed)\n");
      break;
    case NET_GETPROTOBYNAME:
      perror("getprotobyname");
      break;
    case NET_SOCKET:
      perror("socket");
      break;
    case NET_BIND:
      perror("bind");
      break;
    case NET_LISTEN:
      perror("listen");
      break;
    case NET_CONNECT:
      perror("connect");
      break;
    case NET_ACCEPT:
      perror("accept");
      break;
    case NET_RECV:
      perror("recv");
      break;
    case NET_SEND:
      perror("send");
      break;
    case NET_READ:
      perror("read");
      break;
    default:
      fprintf(stderr, "Success\n");
      break;
    }
}
