/*
** socket.c for my_libnetwork in /home/baron_a/Projects/my_libnetwork
**
** Made by Alexandre Baron
** Login   <baron_a@epitech.net>
**
** Started on  Sat Sep  1 23:43:41 2012 Alexandre Baron
** Last update Wed Sep 12 20:30:06 2012 Alexandre Baron
*/
/* Copyright (C) 2012 Baron Alexandre */
/* 	      This program is free software; you can redistribute it and/or */
/* modify it under the terms of the GNU Lesser General Public */
/* 	      License as published by the Free Software Foundation; either */
/* 	      version 2.1 of the License, or (at your option) any later version. */

/* 		   This program is distributed in the hope that it will be useful, */
/* 		   but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU */
/* Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public */
/* 	      License along with this program; if not, write to the Free Software */
/*   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA */


#include <netdb.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "ring_buffer.h"
#include "net_perror.h"
#include "socket.h"
#include "values.h"
#include "globals.h"
#include "communication.h"
#include "log.h"

/* Socket initialization function. Every socket created should pass through it ! */
void		initialize_socket(t_sock *sock, int family, socket_type type)
{
  sock->fd = -1;
  sock->family = family;
  sock->type = type;
  sock->in = NULL;
  sock->out = NULL;
  write_to_log("initialize_socket", "Socket initialization", NULL);
}

/* Function to destroy an active socket (e.g. in case of client deconnection). */
/* - verifies that the given socket isn't null */
/* - closes the socket fd */
/* - free the communications buffers, if any */
int			destroy_socket(t_sock *socket)
{
  extern net_code	g_code;

  if (socket == NULL)
    {
      g_code = NET_INVALID_ARG;
      write_to_log("destroy_socket", "Invalid argument:", "Null socket");
      return (NET_FAILURE);
    }
  if (socket->fd > 0)
    close(socket->fd);
  if (socket->in != NULL)
    rb_free(socket->in);
  if (socket->out != NULL)
    rb_free(socket->out);
  free(socket);
  write_to_log("destroy_socket", "Socket successfully destroyed", NULL);
  return (NET_SUCCESS);
}



/* Purely server-side function, permitting to allow new clients. */
/*   Parameters : */
/*   - serv : the server socket (should not be null) */
/*   - capacity : capacity (in "number of parts") of the ring buffer of the client */
/*   - size : size of those parts */
/* - First accept(2) the client */
/* - allocate and initialize his socket structure */
/* - allocate his communication buffers (since it will be a CLIENT socket, we assume */
/*    that we will want to both read and write on it...) */
t_sock			*accept_socket(t_sock *serv, size_t capacity, size_t size)
{
  t_sock		*new;
  socklen_t		addrlen;
  int			fd;
  struct sockaddr	addr;
  extern net_code	g_code;

  if (serv == NULL)
    {
      g_code = NET_INVALID_ARG;
      write_to_log("accept_socket", "Invalid argument:", "Null server socket");
      return (NULL);
    }
  if ((fd = accept(serv->fd, &(addr), &(addrlen))) == -1)
    {
      g_code = NET_ACCEPT;
      write_to_log("accept_socket", "accept:", strerror(errno));
      return (NULL);
    }
  if ((new = malloc(sizeof(*new))) == NULL)
    {
      g_code = NET_MALLOC;
      write_to_log("accept_socket", "malloc:", "Virtual memory exhausted");
      return (NULL);
    }
  initialize_socket(new, serv->family, CLIENT);
  new->fd = fd;
 if (create_communication_buffers(new, BOTH, capacity, size) == NET_FAILURE)
   {
     destroy_socket(new);
     return (NULL);
   }
 write_to_log("accept_socket", "New client socket accepted", NULL);
 return (new);
}



/* Generic socket enabling function depending on what you want your socket to be */
/* (client or server). */
/* Parameters : */
/* - the socket */
/* - ip : the ip to connect to. For a server socket, pass NULL. */
/* - port : the port to listen on (server) or to connect to (client). */
/* - backlog : specific parameter of listen(2) for the socket's waiting queue size. */
/*   Unless special use, it can be set to SOMAXCONN (128). */
/* Function : */
/* - verifies if socket isn't null */
/* - retrieves socket family and port (family should have been set in create_socket) */
/* - for server socket : accept connexions from any address, bind(2) socket, listen(2) on socket */
/* - for client socket : connect on the specified IP */
/* - in both cases, at the end, the socket is ready for what it has to do. */
int			enable_socket(t_sock *socket, char *ip, int port, int backlog)
{
  struct sockaddr_in	addr;
  extern net_code	g_code;

  if (socket == NULL)
    {
      g_code = NET_INVALID_ARG;
      write_to_log("enable_socket", "Invalid argument:", "Null socket");
      return (NET_FAILURE);
    }
  addr.sin_family = socket->family;
  addr.sin_port = htons(port);
  if (socket->type == SERVER)
    {
      addr.sin_addr.s_addr = INADDR_ANY;
      if (bind(socket->fd, (const struct sockaddr *)&(addr), sizeof(addr)) == -1)
	{
	  g_code = NET_BIND;
	  write_to_log("enable_socket", "bind:", strerror(errno));
	  return (NET_FAILURE);
	}
      if (listen(socket->fd, backlog) == -1)
	{
	  g_code = NET_LISTEN;
	  write_to_log("enable_socket", "listen:", strerror(errno));
	  return (NET_FAILURE);
	}
    }
  else // if socket type == CLIENT
    {
      if (ip == NULL)
	{
	  g_code = NET_INVALID_ARG;
	  write_to_log("enable_socket", "Invalid argument:", "Null IP");
	  return (NET_FAILURE);
	}
      addr.sin_addr.s_addr = inet_addr(ip);
      if (connect(socket->fd, (const struct sockaddr *)&(addr), sizeof(addr)) == -1)
	{
	  g_code = NET_CONNECT;
	  write_to_log("enable_socket", "connect:", strerror(errno));
	  return (NET_FAILURE);
	}
    }
  write_to_log("enable_socket", "Successfully enabled socket", NULL);
  return (NET_SUCCESS);
}





/* Socket creation function. */
/* The library's other functions should be used only with sockets */
/* created via this function ! */
/* Parameters : */
/* - protocol : name of the protocol you want to bind the socket to (usually "TCP") */
/* - family : value defining the kind of connection you want for your socket. */
/* Most usual values are AF_INET (IPv4) and AF_INET6 (IPv6) */
/* - com : type of communications semantics. Most usual value would be SOCK_STREAM */
/* For all possible values of family and com, man socket(2). */
/* - type : "boolean" designed to know if your socket should be server or client. */
/*  type is useful to the enable_socket function. */
/* Function : */
/* - creates a TCP protocol entry */
/* - correctly initializes internal values of socket */
/* - calls socket(2) and store its return value */
t_sock			*create_socket(char *protocol, int family, int com, socket_type type)
{
  struct protoent	*pe;
  t_sock		*new;
  extern net_code	g_code;

  pe = getprotobyname(protocol);
  if (pe == NULL || (type != SERVER && type != CLIENT))
    {
      if (!pe)
	{
	  g_code = NET_GETPROTOBYNAME;
	  write_to_log("create_socket", "getprotobyname:", "Unknown error");
	}
      else
	{
	  g_code = NET_INVALID_ARG;
	  write_to_log("create_socket", "Invalid argument:", "Type is neither SERVER nor CLIENT");
	}
      return (NULL);
    }
  if ((new = malloc(sizeof(*new))) == NULL)
    {
      g_code = NET_MALLOC;
      write_to_log("create_socket", "malloc:", "Virtual memory exhausted");
      return (NULL);
    }
  initialize_socket(new, family, type);
  new->fd = socket(family, com, pe->p_proto);
  if (new->fd == -1)
    {
      g_code = NET_SOCKET;
      write_to_log("create_socket", "socket:", strerror(errno));
      destroy_socket(new);
      new = NULL;
    }
  else
    write_to_log("create_socket", "New socket successfully created", NULL);
  return (new);
}
