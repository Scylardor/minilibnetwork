##
## Makefile for Ring Buffer Library in /home/barona/ring_buffer
## 
## Made by baron_a
## Login   <barona@epitech.net>
## 
## Started on  Thu Aug 30 09:45:14 2012 baron_a
## Last update Mon Sep 10 21:45:32 2012 Alexandre Baron
##
# Copyright (C) 2012 Baron Alexandre

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA




CC=		gcc

NAME=		libnetwork_Linux_x86-64.so

CFLAGS= 	-W -Wall -Wextra -I./inc -c

#CXXFLAGS= 	-W -Wall -I./inc -O3

LDFLAGS= 	

OBJ_DIR= 	./obj

SRC_DIR=	./src

RM=		rm -rf

SRC= 		$(wildcard $(SRC_DIR)/*.c)

OBJ= 		$(subst $(SRC_DIR)/, $(OBJ_DIR)/, $(SRC))

OBJ:= 		$(OBJ:.c=.o)

$(NAME):	$(OBJ)
		@$(CC) -shared -o $(NAME) $(wildcard $(OBJ_DIR)/*.o)
# @$(CC) $^ $(LDFLAGS) -o $@
		@echo "\033[33;36m[CC] creation de $@.\033[33;39m"

all:		$(NAME)

$(OBJ_DIR)/%.o:	$(SRC_DIR)/%.c
		@$(CC) -c -fPIC $< $(INCLUDE) $(CFLAGS) -o $@
		@echo "\033[33;32m[OBJ] creation de $@\033[33;39m"

clean:
		@$(RM) $(OBJ_DIR)/*.o
		@echo "\033[33;31m[RM] suppression des fichiers objets...\033[33;39m"

debug:
		@$(CC) $(CFLAGS) $(SRC) $(LDFLAGS) -ggdb3

fclean:		clean
		@$(RM) $(NAME)
		@echo "\033[33;38m[RM] suppresion de l'executable $(NAME).\033[33;39m"

re:		fclean all

.PHONY: all clean fclean re
