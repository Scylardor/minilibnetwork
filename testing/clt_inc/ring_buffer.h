/*
** ring_buffer.h for Ring Buffer Library in /home/barona/ring_buffer
**
** Made by baron_a
** Login   <barona@epitech.net>
**
** Started on  Thu Aug 30 10:00:42 2012 baron_a
** Last update Sun Sep  2 22:34:44 2012 Alexandre Baron
*/

#ifndef	RING_BUFFER_H_
# define RING_BUFFER_H_

#include <stdlib.h>

typedef		struct s_ring_buffer
{
  void		*start;     // data buffer
  void		*end; // end of data buffer
  size_t	capacity;  // maximum number of items in the buffer
  size_t	count;     // number of items in the buffer
  size_t	size;        // size of each item in the buffer
  size_t	offset;      // to never lose anything while sending
  void		*head;       // pointer to head
  void		*tail;       // pointer to tail
}		t_ring;

t_ring	*rb_create(size_t capacity, size_t size);

#endif
