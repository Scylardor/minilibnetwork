/*
** typedef.h for libnetwork in /home/baron_a/Projects/my_libnetwork
**
** Made by Alexandre Baron
** Login   <baron_a@epitech.net>
**
** Started on  Sat Sep  1 23:52:54 2012 Alexandre Baron
** Last update Sun Sep  2 19:08:40 2012 Alexandre Baron
*/

#ifndef	TYPEDEF_H_
# define TYPEDEF_H_

typedef	enum net_return_values
  {
    NET_FAILURE = -1,
    NET_SUCCESS	= 0,
    NET_INVALID_ARG = 1,
    NET_FULLBUFF = 2,
    NET_EMPTYBUFF = 3
  }	net_return_value;

typedef	enum buff_to_alloc
  {
    IN,
    OUT,
    BOTH
  }	buff_to_alloc;

#endif
