/*
** socket.h for my_libnetwork in /home/baron_a/Projects/my_libnetwork
**
** Made by Alexandre Baron
** Login   <baron_a@epitech.net>
**
** Started on  Sat Sep  1 23:43:56 2012 Alexandre Baron
** Last update Sun Sep  2 22:34:29 2012 Alexandre Baron
*/

#ifndef	SOCKET_H_
# define SOCKET_H_

typedef	enum socket_type
  {
    SERVER,
    CLIENT
  }	socket_type;

typedef		struct s_socket
{
  int		fd;
  socket_type	type;
  int		family;
  t_ring	*in;
  t_ring	*out;

}	t_sock;

void	initialize_socket(t_sock *sock, int family, socket_type type);
int	destroy_socket(t_sock *socket);
int	accept_socket(t_sock *serv, t_sock *new);
int	enable_socket(t_sock *socket, char *ip, int port, int backlog);
t_sock	*create_socket(char *protocol, int family, int com, socket_type type);

#endif
