/*
** main.c for Ring Buffer Library in /home/barona/ring_buffer
**
** Made by baron_a
** Login   <barona@epitech.net>
**
** Started on  Thu Aug 30 09:50:30 2012 baron_a
** Last update Sun Sep  2 23:39:41 2012 Alexandre Baron
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>

#include "ring_buffer.h"
#include "socket.h"
#include "values.h"

void		ecoute(t_sock *sock)
{
  /* int		nfds; */
  /* fd_set	readfds; */
  /* fd_set	writefds; */
  /* int		fds[50]; */
  /* int		pos; */

  /* nfds = sock->fd + 1; */
  /* pos = 0; */
  /* while (1) */
  /*   { */
  /*     FD_SET(sock->fd, &(readfds)); */
  /*     if (pos > 0) */
  /* 	{ */
  /* 	  monitor_clients(fds, &readfds, &writefds, pos); */
  /* 	} */
  /*     if (select(nfds, &(readfds), &(writefds), NULL, NULL) == -1) */
  /* 	perror("select"); */
  /*     if (FD_ISSET(sock->fd, &readfds)) */
  /* 	printf("new client\n"); */
  /*   } */
}

int		main()
{
  t_sock	*sock;

  sock = create_socket("TCP", AF_INET, SOCK_STREAM, CLIENT);
  if (sock == NULL)
    {
      net_perror(NET_FAILURE);
    }
  else
    {
      if (enable_socket(sock, "127.0.0.1", 4242, 42) != NET_SUCCESS)
	net_perror(NET_FAILURE);
      else
	ecoute(sock);
      destroy_socket(sock);
    }
  return (0);
}
