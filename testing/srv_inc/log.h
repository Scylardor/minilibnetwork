/*
** log.h for libnetwork in /home/baron_a/Projects
**
** Made by Alexandre Baron
** Login   <baron_a@epitech.net>
**
** Started on  Tue Sep  4 17:47:26 2012 Alexandre Baron
** Last update Mon Sep 10 21:46:04 2012 Alexandre Baron
*/
/* Copyright (C) 2012 Baron Alexandre */
/* 	      This program is free software; you can redistribute it and/or */
/* modify it under the terms of the GNU Lesser General Public */
/* 	      License as published by the Free Software Foundation; either */
/* 	      version 2.1 of the License, or (at your option) any later version. */

/* 		   This program is distributed in the hope that it will be useful, */
/* 		   but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU */
/* Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public */
/* 	      License along with this program; if not, write to the Free Software */
/*   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA */

#ifndef	LOG_H_
#define	LOG_H_

typedef	enum net_log_code
  {
    LOG_ON,
    LOG_ON_APPEND,
    LOG_OFF
  }	net_log_code;

void		write_to_log(char *func, char *what, char *opt);
int		net_log(net_log_code code);

#endif /* !LOG_H_ */
