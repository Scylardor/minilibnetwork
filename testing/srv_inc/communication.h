/*
** communication.h for my_libnetwork in /home/baron_a/Projects/my_libnetwork
**
** Made by Alexandre Baron
** Login   <baron_a@epitech.net>
**
** Started on  Sun Sep  2 19:58:53 2012 Alexandre Baron
** Last update Wed Sep 12 20:39:13 2012 Alexandre Baron
*/
/* Copyright (C) 2012 Baron Alexandre */
/* 	      This program is free software; you can redistribute it and/or */
/* modify it under the terms of the GNU Lesser General Public */
/* 	      License as published by the Free Software Foundation; either */
/* 	      version 2.1 of the License, or (at your option) any later version. */

/* 		   This program is distributed in the hope that it will be useful, */
/* 		   but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU */
/* Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public */
/* 	      License along with this program; if not, write to the Free Software */
/*   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA */

#ifndef	COMMUNICATION_H_
#define	COMMUNICATION_H_

int	create_communication_buffers(t_sock *socket, buff_to_alloc value, size_t capacity, size_t size);
int	read_from_socket(t_sock *sock, int flags);
int	send_to_socket(t_sock *sock, int flags);

#endif /* !COMMUNICATION_H_ */
