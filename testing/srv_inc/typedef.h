/*
** typedef.h for libnetwork in /home/baron_a/Projects/my_libnetwork
**
** Made by Alexandre Baron
** Login   <baron_a@epitech.net>
**
** Started on  Sat Sep  1 23:52:54 2012 Alexandre Baron
** Last update Mon Sep 10 21:46:47 2012 Alexandre Baron
*/
/* Copyright (C) 2012 Baron Alexandre */
/* 	      This program is free software; you can redistribute it and/or */
/* modify it under the terms of the GNU Lesser General Public */
/* 	      License as published by the Free Software Foundation; either */
/* 	      version 2.1 of the License, or (at your option) any later version. */

/* 		   This program is distributed in the hope that it will be useful, */
/* 		   but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU */
/* Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public */
/* 	      License along with this program; if not, write to the Free Software */
/*   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA */

#ifndef	TYPEDEF_H_
# define TYPEDEF_H_

# define	NET_FAILURE	-1
# define	NET_SUCCESS	0
# define	NET_INVALID_ARG	1

#endif
