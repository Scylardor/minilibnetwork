/*
** server.c for test server in /home/baron_a/Projects/my_libnetwork
**
** Made by Alexandre Baron
** Login   <baron_a@epitech.net>
**
** Started on  Wed Sep 12 20:32:38 2012 Alexandre Baron
** Last update Wed Sep 12 21:35:17 2012 Alexandre Baron
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <string.h>

#include "net_perror.h"
#include "ring_buffer.h"
#include "socket.h"
#include "communication.h"
#include "net_perror.h"
#include "values.h"
#include "log.h"

char	monitor_fds(t_sock *client, fd_set *read, fd_set *write)
{
  int	ret;

  ret = 1;
  if (FD_ISSET(client->fd, read))
    {
      ret = read_from_socket(client, 0);
      if (ret == NET_FAILURE)
	{
	  net_perror();
	  return (-1);
	}
      else
	{
	  if (ret == 0)
	    printf("disconnect\n");
	  rb_push_back(client->out, "Ping? Pong!\n");
	  printf("count: %lu\n", client->out->count);
	}
    }
  if (FD_ISSET(client->fd, write))
    {
      ret = send_to_socket(client, 0);
      if (ret == NET_FAILURE)
	{
	  net_perror();
	  return (-1);
	}
      else
	ret = 1;
    }
  return (ret);
}

void		ecoute(t_sock *sock)
{
  int		nfds;
  fd_set	readfds;
  fd_set	writefds;
  t_sock	*new;
  char		quit;

  nfds = sock->fd + 1;
  new = NULL;
  quit = 0;
  while (!quit)
    {
      FD_ZERO(&readfds);
      FD_ZERO(&writefds);
      FD_SET(sock->fd, &(readfds));
      if (new != NULL)
	{
	  FD_SET(new->fd, &readfds);
	  if (new->out->count > 0)
	    FD_SET(new->fd, &writefds);
	}
      if (select(nfds, &(readfds), &(writefds), NULL, NULL) == -1)
	perror("select");
      if (new != NULL)
	{
	  if ((monitor_fds(new, &readfds, &writefds)) <= 0)
	    {
	      destroy_socket(new);
	      nfds--;
	      new = NULL;
	    }
	}
      if (FD_ISSET(sock->fd, &readfds))
	{
	  if (new)
	    quit = 1;
	  else
	    {
	      new = accept_socket(sock, 10, 128);
	      if (new)
		{
		  rb_push_back(new->out, "Welcome\n");
		  nfds++;
		}
	    }
	}
    }
  destroy_socket(new);
}

int		main()
{
  t_sock	*sock;

  net_log(LOG_ON);
  sock = create_socket("TCP", AF_INET, SOCK_STREAM, SERVER);
  if (sock == NULL)
    {
      net_perror(NET_FAILURE);
    }
  else
    {
      if (enable_socket(sock, NULL, 4243, 42) != NET_SUCCESS)
	net_perror(NET_FAILURE);
      else
	ecoute(sock);
      destroy_socket(sock);
      net_log(LOG_OFF);
    }
  return (0);
}
