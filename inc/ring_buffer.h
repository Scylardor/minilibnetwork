/*
** ring_buffer.h for Ring Buffer Library in /home/barona/ring_buffer
**
** Made by baron_a
** Login   <barona@epitech.net>
**
** Started on  Thu Aug 30 10:00:42 2012 baron_a
** Last update Mon Sep 10 21:46:35 2012 Alexandre Baron
*/
/* Copyright (C) 2012 Baron Alexandre */
/* 	      This program is free software; you can redistribute it and/or */
/* modify it under the terms of the GNU Lesser General Public */
/* 	      License as published by the Free Software Foundation; either */
/* 	      version 2.1 of the License, or (at your option) any later version. */

/* 		   This program is distributed in the hope that it will be useful, */
/* 		   but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU */
/* Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public */
/* 	      License along with this program; if not, write to the Free Software */
/*   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA */

#ifndef	RING_BUFFER_H_
# define RING_BUFFER_H_

#include <stdlib.h>

typedef	enum buff_to_alloc
  {
    IN,
    OUT,
    BOTH
  }	buff_to_alloc;


typedef		struct s_ring_buffer
{
  void		*start;     // data buffer
  void		*end; // end of data buffer
  size_t	capacity;  // maximum number of items in the buffer
  size_t	count;     // number of items in the buffer
  size_t	size;        // size of each item in the buffer
  size_t	offset;      // to never lose anything while sending
  void		*head;       // pointer to head
  void		*tail;       // pointer to tail
}		t_ring;

t_ring	*rb_create(size_t capacity, size_t size);
void	rb_free(t_ring *buff);
int	rb_push_back(t_ring *buff, const void *item);
int	rb_pop_front(t_ring *buff, void *item);

#endif
