/*
** socket.h for my_libnetwork in /home/baron_a/Projects/my_libnetwork
**
** Made by Alexandre Baron
** Login   <baron_a@epitech.net>
**
** Started on  Sat Sep  1 23:43:56 2012 Alexandre Baron
** Last update Mon Sep 10 21:46:40 2012 Alexandre Baron
*/
/* Copyright (C) 2012 Baron Alexandre */
/* 	      This program is free software; you can redistribute it and/or */
/* modify it under the terms of the GNU Lesser General Public */
/* 	      License as published by the Free Software Foundation; either */
/* 	      version 2.1 of the License, or (at your option) any later version. */

/* 		   This program is distributed in the hope that it will be useful, */
/* 		   but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU */
/* Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public */
/* 	      License along with this program; if not, write to the Free Software */
/*   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA */

#ifndef	SOCKET_H_
# define SOCKET_H_

typedef	enum socket_type
  {
    SERVER,
    CLIENT
  }	socket_type;

typedef		struct s_socket
{
  int		fd;
  socket_type	type;
  int		family;
  t_ring	*in;
  t_ring	*out;
}	t_sock;

void	initialize_socket(t_sock *sock, int family, socket_type type);
int	destroy_socket(t_sock *socket);
int	enable_socket(t_sock *socket, char *ip, int port, int backlog);
t_sock	*accept_socket(t_sock *serv, size_t capacity, size_t size);
t_sock	*create_socket(char *protocol, int family, int com, socket_type type);

#endif
